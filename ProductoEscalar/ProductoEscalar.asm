;%include "io.inc"

global main
extern printf

section .data
    puntero dd 25.0,5.0,3.0,8.0
    cantPunteros dd 4   ;valor que indica la cantidad de valores del vector (puntero)
    r dd 2.0
    formato db "el numero es : %f", 10,13,0
    result dq 0.0

section .text
main:
    mov ebp, esp; for correct debugging
    ;enter
    push ebp
    mov ebp, esp
    mov esi, 0
producto_rvf:    
    ;calculamos el valor de multiplicar un elemento del puntero por r
    fld dword [puntero+(esi*4)]
    fld dword [r]
    fmul 
    fstp qword [result]   
     
    ;Imprimimos el valor:
    push dword [result+4]
    push dword [result]
    push formato
    call printf
    add esp, 12
    
    ;incrementamos el valor de esi y si es igual al limite de valores del vector salta a fin, sino salta de nuevo a producto_rvf
    inc esi
    cmp esi, [cantPunteros]
    je fin
    jmp producto_rvf
    
    
fin:
    ;leave
    mov esp, ebp
    pop ebp
    ;write your code here
    xor eax, eax
    ret
