# Tp-01-Orga-2

# Formula Resolvente:

1_ Para la ejecución del programa de la formula resolvente, se tiene que primero compilar y obtenemos el codigo objeto (.o):

>_ nasm -f elf32 FormulaResolvente.asm -o FormulaResolvente.o;

2_ Luego Linkear y con eso obtenemos el ejecutable (Programa):

>_ gcc -m32 -o Programa FormulaResolvente.o Programa.c

Por lo que ahora tendriamos los siguientes archivos en nuestro directorio:

![01_Ejecutable_Programa](/uploads/4b26ee0cbd505f25b511240ae09943b5/01_Ejecutable_Programa.PNG)

3_ Y por ultimo la ejecucion del programa:

>_ ./Programa

4_ Ahora que ya ejecutamos el programa, nos pedira que ingresemos los 3 valores de a, b y c:

![02_Nos_pide_los_valores_de_a__b_y_c_por_consola](/uploads/b9d0a2875d0923f8fc1c74910fa84f28/02_Nos_pide_los_valores_de_a__b_y_c_por_consola.PNG)

5_ Una vez insertados los valores se ejecutara la formula resolvente e imprimira por pantalla las raices obtenidas:

![03_Luego_imprime_por_pantalla_las_raices_del_problema](/uploads/2bd2908a5a74fdd8f0fd25c0b91f35ba/03_Luego_imprime_por_pantalla_las_raices_del_problema.PNG)


# Producto Escalar:

1_ Para la ejecución del programa que genera el producto escalar, se tienen que ejecutar los siguientes comandos:

El primero genera el codigo objeto (.o):

>_ nasm -f elf ProductoEscalar.asm

El segundo genera el ejecutable:

>_ gcc -m32 ProductoEscalar.o -o ProductoEscalar

Una vez ejecutado esos comandos, se tienen los siguientes objetos:

![04_producto_escalar](/uploads/261cd310f4ff300cf89ebf4de5844e06/04_producto_escalar.PNG)

2_ Una vez ejecutado lo anterior, ahora tenemos generado el ejecutable ProductoEscalar, que lo ejecutamos con:

>_ ./ProductoEscalar

Probamos con los valores:

![05_valores_de_variables_producto_escalar](/uploads/d60a47b3092f86f6b920e0563b4fb603/05_valores_de_variables_producto_escalar.PNG)

El valor de cantPunteros, lo usamos para determinar la cantidad de valores que tiene el vector (puntero).

Y como resultado obtuvimos:

![06_resultado](/uploads/8f228e605f39abda79cf9dbb50574c80/06_resultado.PNG)


