;%include "io.inc"
extern printf

section .data
    format db "La raiz es: %f",10,0
    cuatro dq 4.0
    dos dq 2.0
    menosuno dq -1.0
    a dq 0.0
    b dq 0.0
    c dq 0.0
    resultParcial1 dq 0.0
    resultParcial2 dq 0.0
    result1 dq 0.0
    result2 dq 0.0
section .text
global CMAIN
CMAIN:
     mov ebp, esp
    ;enter
    push ebp
    mov ebp, esp

    ;--- Tomamos los valores de a,b,c pasados por la clase C, y guardamos los valores en las variables a,b,c
    fld dword[ebp + 8]  ; en ebp + 8, tenemos al float "a"
    fstp qword[a]        ; guardamos el valor del float "a" en la variable a
    fld dword[ebp + 12] ; en ebp + 12, tenemos al float "b"
    fstp qword[b]        ; guardamos el valor del float "b" en la variable b
    fld dword[ebp + 16] ; en ebp + 16, tenemos al float "c"
    fstp qword[c]        ; guardamos el valor del float "c" en la variable c

    ;--- A continuacion realizamos los calculos de la formula resolvente:
    
    ;-- Hacemos b*b y lo almacenamos en resultParcial1
    fld qword[b]
    fld qword[b]
    fmul
    fstp qword[resultParcial1] 

    ; Hacemos 4*a*c y lo almacenamos en resultParcial2
    fld qword[cuatro]
    fld qword[a]
    fmul
    fld qword[c]
    fmul
    fstp qword[resultParcial2] 

    ; Hacemos la resta de b*b - 4*a*c 
    fld qword[resultParcial1]
    fld qword[resultParcial2]
    fsub
    ; Calculamos la raiz del resultado anterior y lo almacenamos en resultParcial1
    fsqrt
    fstp qword[resultParcial1]

    ; Calculamos la parte del dividendo 2*a y lo almacenamos en resultParcial2
    fld qword[dos]
    fld qword[a]
    fmul
    fstp qword[resultParcial2]

    ; Ahora vamos a calcular el primer resultado con la funcion en positivo -b + sqrt(b*b - 4*a*c) y 
    ; luego lo dividimos por el resultParcial2 y lo almacenamos en resul1
    fld qword[resultParcial1]
    fld qword[b]
    fsub
    fld qword[resultParcial2]
    fdiv
    fstp qword[result1]
 
    ; Ahora vamos a calcular el primer resultado con la funcion en negativo -b - sqrt(b*b - 4*a*c)  y 
    ; luego lo dividimos por el resultParcial2 y lo almacenamos en resul2
    fld qword[menosuno]
    fld qword[resultParcial1]
    fmul
    fld qword[b]
    fsub
    fld qword[resultParcial2]
    fdiv
    fstp qword[result2]
    
    ;--- Mostramos por pantalla los resultados
    push dword[result1+4]
    push dword[result1]
    push format
    call printf
    push dword[result2+4]
    push dword[result2]
    push format
    call printf
    add esp,12  
    
    ;leave
    mov esp, ebp
    ret