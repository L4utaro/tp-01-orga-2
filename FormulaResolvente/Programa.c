#include <stdio.h>
#include <stdlib.h>

extern void CMAIN(float a, float b, float c);

float ingresar_numero_punto_flotante(char caracter) {
    float numero;
    printf("Introduzca un numero entero para la variable %c : ", caracter);
    scanf("%f", &numero);
    return numero;
}

int main() {
    /*** Primero le pedimos al usuario que ingrese los numeros para las variables a, b y c***/
    float a = ingresar_numero_punto_flotante('a');
    float b = ingresar_numero_punto_flotante('b');
    float c = ingresar_numero_punto_flotante('c');
    /*** Ahora llamomos a la clase asm y le pasamos los 3 valores float, cuando termine imprimira por consola los dos valores ***/
    CMAIN(a, b, c);
    return 0;
}